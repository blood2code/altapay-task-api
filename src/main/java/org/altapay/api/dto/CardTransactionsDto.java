package org.altapay.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardTransactionsDto {
    private Long id;
    private UserDto user;
    private CardsDto cards;
    private String type;
    private Long amount;
    private Long oldBalance;
    private Long newBalance;
    private Timestamp dt;

}
