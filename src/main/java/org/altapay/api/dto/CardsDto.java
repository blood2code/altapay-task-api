package org.altapay.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardsDto {
    private Long id;
    private String card_name;
    private String card_type;
    private String card_number;
    private String card_expire;
    private Long balance;
    private Integer status;
    private Timestamp dt;
    private UserDto user;
    private List<CardTransactionsDto> transactions;
}
