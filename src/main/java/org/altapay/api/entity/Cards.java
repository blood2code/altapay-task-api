package org.altapay.api.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.CurrentTimestamp;

import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

@Entity(name = "cards")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cards {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARDS_SEQUENCE")
    @SequenceGenerator(sequenceName = "CARDS_SEQUENCE", allocationSize = 1, name = "CARDS_SEQUENCE")
    private Long id;

    private String card_name;

    @Size(max = 3)
    @Check(constraints = "card_type IN ('uz', 'hum')")
    private String card_type;

    @Size(max = 20)
    private String card_number;

    @Size(max = 7)
    private String card_expire;

    private Long balance;

    private Integer status;

    @CurrentTimestamp
    private Timestamp dt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "cards")
    private List<CardTransactions> cardTransactions;

}
