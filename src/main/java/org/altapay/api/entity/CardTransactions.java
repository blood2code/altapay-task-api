package org.altapay.api.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.CurrentTimestamp;

import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity(name = "card_transactions")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardTransactions {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARD_TRANSACTIONS_SEQUENCE")
    @SequenceGenerator(sequenceName = "CARD_TRANSACTIONS_SEQUENCE", allocationSize = 1, name = "CARD_TRANSACTIONS_SEQUENCE")
    private Long id;

    @Size(max = 6)
    @Check(constraints = "type IN ('debit', 'credit')")
    private String type;

    private Long amount;

    private Long oldBalance;

    private Long newBalance;

    @CurrentTimestamp
    private Timestamp dt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "card_id")
    private Cards cards;
}
