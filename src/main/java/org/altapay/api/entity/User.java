package org.altapay.api.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CurrentTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_SEQUENCE")
    @SequenceGenerator(sequenceName = "USERS_SEQUENCE", allocationSize = 1, name = "USERS_SEQUENCE")
    private Long id;

    @Column(nullable = false)
    private String fio;

    private String email;

    private Long phone;

    private Integer status;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date dob;

    @CurrentTimestamp
    private Timestamp dt;

    @OneToMany(mappedBy = "user")
    private List<Cards> cards;
}
