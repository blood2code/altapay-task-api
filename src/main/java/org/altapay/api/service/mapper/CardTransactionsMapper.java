package org.altapay.api.service.mapper;

import org.altapay.api.dto.CardTransactionsDto;
import org.altapay.api.entity.CardTransactions;

public class CardTransactionsMapper {
    public static CardTransactionsDto toDto(CardTransactions cardTransactions) {
        return CardTransactionsDto.builder()
                .id(cardTransactions.getId())
                .cards(CardsMapper.toDto(cardTransactions.getCards()))
                .type(cardTransactions.getType())
                .user(UserMapper.toDto(cardTransactions.getUser()))
                .amount(cardTransactions.getAmount())
                .oldBalance(cardTransactions.getOldBalance())
                .newBalance(cardTransactions.getNewBalance())
                .dt(cardTransactions.getDt())
                .build();
    }

    public static CardTransactionsDto toDtoWithoutCards(CardTransactions cardTransactions) {
        return CardTransactionsDto.builder()
                .id(cardTransactions.getId())
                .type(cardTransactions.getType())
                .amount(cardTransactions.getAmount())
                .oldBalance(cardTransactions.getOldBalance())
                .newBalance(cardTransactions.getNewBalance())
                .dt(cardTransactions.getDt())
                .build();
    }
}
