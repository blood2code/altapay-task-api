package org.altapay.api.service.mapper;

import org.altapay.api.dto.CardsDto;
import org.altapay.api.entity.Cards;

public class CardsMapper {
    public static CardsDto toDto(Cards cards) {
        return CardsDto.builder()
                .id(cards.getId())
                .dt(cards.getDt())
                .card_expire(cards.getCard_expire())
                .card_name(cards.getCard_name())
                .card_number(cards.getCard_number())
                .card_type(cards.getCard_type())
                .status(cards.getStatus())
                .balance(cards.getBalance())
                .user(UserMapper.toDtoWithoutCards(cards.getUser()))
                .transactions(cards.getCardTransactions().stream().map(CardTransactionsMapper::toDto).toList())
                .build();
    }

    public static CardsDto CardsDtoWithoutUser(Cards cards) {
        return CardsDto.builder()
                .id(cards.getId())
                .dt(cards.getDt())
                .card_expire(cards.getCard_expire())
                .card_name(cards.getCard_name())
                .card_number(cards.getCard_number())
                .card_type(cards.getCard_type())
                .status(cards.getStatus())
                .balance(cards.getBalance())
                .transactions(cards.getCardTransactions().stream().map(CardTransactionsMapper::toDtoWithoutCards).toList())
                .build();
    }
}
