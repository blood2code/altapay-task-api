package org.altapay.api.service.mapper;

import org.altapay.api.dto.UserDto;
import org.altapay.api.entity.User;

public class UserMapper {

    public static UserDto toDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .dt(user.getDt())
                .dob(user.getDob())
                .fio(user.getFio())
                .email(user.getEmail())
                .cardsDtoList(user.getCards().stream().map(CardsMapper::CardsDtoWithoutUser).toList())
                .phone(user.getPhone())
                .status(user.getStatus())
                .build();
    }

    public static UserDto toDtoWithoutCards(User user) {
        return UserDto.builder()
                .id(user.getId())
                .dt(user.getDt())
                .dob(user.getDob())
                .fio(user.getFio())
                .email(user.getEmail())
                .phone(user.getPhone())
                .status(user.getStatus())
                .build();
    }

}
