package org.altapay.api.service;

import lombok.RequiredArgsConstructor;
import org.altapay.api.dto.ResponseDto;
import org.altapay.api.dto.UserDto;
import org.altapay.api.entity.User;
import org.altapay.api.repository.UserRepository;
import org.altapay.api.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public ResponseEntity<ResponseDto<List<UserDto>>> getUserInfo() {
        List<User> userOptional = userRepository.findAll();

        if (!userOptional.isEmpty()) {
            List<UserDto> userDto = userOptional.stream().map(UserMapper::toDto).toList();
            return ResponseEntity.ok().body(new ResponseDto<>(
                    "Users found",
                    userDto
            ));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
