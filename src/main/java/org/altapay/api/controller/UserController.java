package org.altapay.api.controller;

import lombok.RequiredArgsConstructor;
import org.altapay.api.dto.ResponseDto;
import org.altapay.api.dto.UserDto;
import org.altapay.api.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @GetMapping("/users-info")
    public ResponseEntity<ResponseDto<List<UserDto>>> getUserInfo() {
        return userService.getUserInfo();
    }
}
