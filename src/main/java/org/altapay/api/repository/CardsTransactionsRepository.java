package org.altapay.api.repository;

import org.altapay.api.entity.CardTransactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardsTransactionsRepository extends JpaRepository<CardTransactions, Long> {
}
