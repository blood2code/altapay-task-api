package org.altapay.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AltapayTaskApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(AltapayTaskApiApplication.class, args);
    }

}
